﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HomeInventory.Models
{
    public class PurchaseInfo
    {
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PurchaseInfoId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Purchase Date")]
        public DateTime When { get; set; }

        [StringLength(255, ErrorMessage = "Where purchase info cannot be longer than 255 characters.")]
        public string Where { get; set; }

        [StringLength(255, ErrorMessage = "Warranty info cannot be longer than 255 characters.")]
        public string Warranty { get; set; }

        [DataType(DataType.Currency)]
        public double Price { get; set; }

        public virtual HomeItem HomeItem { get; set; }

    }
}