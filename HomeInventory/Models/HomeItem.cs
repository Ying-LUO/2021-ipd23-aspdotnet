﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HomeInventory.Models
{
    public class HomeItem
    {
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HomeItemId { get; set; }

        [StringLength(100, ErrorMessage = "Model cannot be longer than 100 characters.")]
        public string Model { get; set; }

        [StringLength(100, ErrorMessage = "SerialNumber cannot be longer than 100 characters.")]
        public string SerialNumber { get; set; }

        [ForeignKey("Location")]
        public int LocationId { get; set; }

        public virtual Location Location { get; set; }

        //[ForeignKey("PurchaseInfo")]
        public int PurchaseInfoId { get; set; }

        [Required]
        public PurchaseInfo PurchaseInfo { get; set; }

        [Required(ErrorMessage = "Description cannot be empty", AllowEmptyStrings = false)]
        [StringLength(255, ErrorMessage = "Description cannot be longer than 255 characters.")]
        public string Description { get; set; }

        public byte[] Photo { get; set; }

    }
}