﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HomeInventory.Models
{
    public class Location
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LocationId { get; set; }

        [Display(Name = "Location")]
        [Required(ErrorMessage = "Location cannot be empty", AllowEmptyStrings = false)]
        public string Name { get; set; }

    }
}