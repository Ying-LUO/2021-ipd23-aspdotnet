namespace HomeInventory.Migrations
{
    using HomeInventory.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<HomeInventory.Instraestructure.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(HomeInventory.Instraestructure.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Locations.AddOrUpdate(l => l.Name,
                new Location()
                {
                    Name = "Home Office"
                },
                new Location()
                {
                    Name = "Family Room"
                },
                new Location()
                {
                    Name = "Living Room"
                },
                new Location()
                {
                    Name = "Kitchen"
                },
                new Location()
                {
                    Name = "Master Bedroom"
                },
                new Location()
                {
                    Name = "Bedroom Two"
                },
                new Location()
                {
                    Name = "Bedroom Three"
                },
                new Location()
                {
                    Name = "Bathrooms"
                },
                new Location()
                {
                    Name = "Garage"
                },
                new Location()
                {
                    Name = "Attic"
                },
                new Location()
                {
                    Name = "Basement"
                },
                new Location()
                {
                    Name = "Other"
                }
           );
            context.SaveChanges();
            base.Seed(context);

        }
    }
}
