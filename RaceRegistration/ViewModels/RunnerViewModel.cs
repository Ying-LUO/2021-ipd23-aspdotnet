﻿using RaceRegistration.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RaceRegistration.ViewModels
{
    public class RunnerViewModel
    {
        public int RunnerId { get; set; }

        [StringLength(100)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [StringLength(100)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public DateTime? BirthDate { get; set; }

        public Gender Gender { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(15)]
        public string Telephone { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(15)]
        public string PostalCode { get; set; }

        [Required]
        [Display(Name = "Country")]
        public string SelectedCountryIso3 { get; set; }
        public IEnumerable<SelectListItem> Countries { get; set; }

        public DateTime? RegistrationDate { get; set; }

        public string ContactPersonName { get; set; }

        public string ContactPersonTelephone { get; set; }

        [Required]
        [Display(Name = "Event Name")]
        public string SelectedEvent { get; set; }
        public IEnumerable<SelectListItem> Events { get; set; }

    }
}