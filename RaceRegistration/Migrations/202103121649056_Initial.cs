namespace RaceRegistration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        CountryId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.CountryId);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        EventId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        EventDate = c.DateTime(),
                        IsClosed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.EventId);
            
            CreateTable(
                "dbo.Runners",
                c => new
                    {
                        RunnerId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(maxLength: 100),
                        LastName = c.String(maxLength: 100),
                        BirthDate = c.DateTime(),
                        Gender = c.Int(nullable: false),
                        Email = c.String(maxLength: 50),
                        Telephone = c.String(maxLength: 15),
                        Address = c.String(maxLength: 255),
                        City = c.String(maxLength: 50),
                        PostalCode = c.String(maxLength: 15),
                        CountryId = c.Int(nullable: false),
                        RegistrationDate = c.DateTime(),
                        ContactPersonName = c.String(),
                        ContactPersonTelephone = c.String(),
                        EventId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RunnerId)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: true)
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: true)
                .Index(t => t.CountryId)
                .Index(t => t.EventId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Runners", "EventId", "dbo.Events");
            DropForeignKey("dbo.Runners", "CountryId", "dbo.Countries");
            DropIndex("dbo.Runners", new[] { "EventId" });
            DropIndex("dbo.Runners", new[] { "CountryId" });
            DropTable("dbo.Runners");
            DropTable("dbo.Events");
            DropTable("dbo.Countries");
        }
    }
}
