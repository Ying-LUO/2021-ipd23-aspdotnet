﻿namespace RaceRegistration.Migrations
{
    using RaceRegistration.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RaceRegistration.Infrastructure.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RaceRegistration.Infrastructure.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            context.Countries.AddOrUpdate(l => l.Name,
                new Country()
                {
                    Name = "Albania"
                },
                new Country()
                {
                    Name = "Canada"
                },
                new Country()
                {
                    Name = "Barbados"
                },
                new Country()
                {
                    Name = "Cuba"
                },
                new Country()
                {
                    Name = "Dominica"
                },
                new Country()
                {
                    Name = "Haiti"
                },
                new Country()
                {
                    Name = "Iran"
                },
                new Country()
                {
                    Name = "Israel"
                },
                new Country()
                {
                    Name = "Jordan"
                },
                new Country()
                {
                    Name = "Malaysia"
                },
                new Country()
                {
                    Name = "Libya"
                },
                new Country()
                {
                    Name = "Mexico"
                }
           );
            context.SaveChanges();
            base.Seed(context);
        }
    }
}
