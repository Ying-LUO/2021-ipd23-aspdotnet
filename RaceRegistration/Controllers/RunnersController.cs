﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using RaceRegistration.Infrastructure;
using RaceRegistration.Models;

namespace RaceRegistration.Controllers
{
    public class RunnersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Search(string query)
        {
            var result = db.Runners.AsQueryable()
                .Where(c => c.FirstName.ToLower().Contains(query.ToLower()) ||
                        c.LastName.ToLower().Contains(query.ToLower()))
                .ToList();
            return this.PartialView("_RunnersList", result);
        }

        // GET: Runners
        public ActionResult Index(string sortDir, string searchString, string currentFilter, int? page, string sortOrder = "")
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.currentFilter = searchString;
            ViewBag.sortOrder = sortOrder;
            ViewBag.sortDir = sortDir;

            var runners = db.Runners.AsQueryable();

            if (!string.IsNullOrEmpty(searchString))
            {
                runners = runners.Where(p => p.FirstName.Contains(searchString) || p.LastName.Contains(searchString));
            }

            switch (sortOrder.ToLower())
            {
                case "name":
                    if (sortDir.ToLower() == "desc")
                        runners = runners.OrderByDescending(s => s.FirstName);
                    else
                        runners = runners.OrderBy(s => s.FirstName);
                    break;
                case "gender":
                    if (sortDir.ToLower() == "desc")
                        runners = runners.OrderByDescending(s => s.Gender);
                    else
                        runners = runners.OrderBy(s => s.Gender);
                    break;
                case "email":
                    if (sortDir.ToLower() == "desc")
                        runners = runners.OrderByDescending(s => s.Email);
                    else
                        runners = runners.OrderBy(s => s.Email);
                    break;
                case "telephone":
                    if (sortDir.ToLower() == "desc")
                        runners = runners.OrderByDescending(s => s.Telephone);
                    else
                        runners = runners.OrderBy(s => s.Telephone);
                    break;
                case "eventname":
                    if (sortDir.ToLower() == "desc")
                        runners = runners.OrderByDescending(s => s.Event.Name);
                    else
                        runners = runners.OrderBy(s => s.Event.Name);
                    break;
                case "eventstatus":
                    if (sortDir.ToLower() == "desc")
                        runners = runners.OrderByDescending(s => s.Event.IsClosed);
                    else
                        runners = runners.OrderBy(s => s.Event.IsClosed);
                    break;
                default:
                    runners = runners.OrderBy(s => s.FirstName);
                    break;
            }

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            var data = runners.ToPagedList(pageNumber, pageSize);
            if (Request.IsAjaxRequest())
            {
                return this.PartialView("_RunnersList", data);
            }
            else
            {
                return View(data);
            }
        }

        // GET: Runners/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Runner runner = db.Runners.Find(id);
            if (runner == null)
            {
                return HttpNotFound();
            }
            return View(runner);
        }

        // GET: Runners/Create
        public ActionResult Create()
        {
            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "Name");
            ViewBag.EventId = new SelectList(db.Events, "EventId", "Name");
            return View();
        }

        // POST: Runners/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RunnerId,FirstName,LastName,BirthDate,Gender,Email,Telephone,Address,City,PostalCode,CountryId,RegistrationDate,ContactPersonName,ContactPersonTelephone,EventId")] Runner runner)
        {
            if (ModelState.IsValid)
            {
                db.Runners.Add(runner);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "Name", runner.CountryId);
            ViewBag.EventId = new SelectList(db.Events, "EventId", "Name", runner.EventId);
            return View(runner);
        }

        // GET: Runners/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Runner runner = db.Runners.Find(id);
            if (runner == null)
            {
                return HttpNotFound();
            }
            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "Name", runner.CountryId);
            ViewBag.EventId = new SelectList(db.Events, "EventId", "Name", runner.EventId);
            return View(runner);
        }

        // POST: Runners/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RunnerId,FirstName,LastName,BirthDate,Gender,Email,Telephone,Address,City,PostalCode,CountryId,RegistrationDate,ContactPersonName,ContactPersonTelephone,EventId")] Runner runner)
        {
            if (ModelState.IsValid)
            {
                db.Entry(runner).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "Name", runner.CountryId);
            ViewBag.EventId = new SelectList(db.Events, "EventId", "Name", runner.EventId);
            return View(runner);
        }

        // GET: Runners/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Runner runner = db.Runners.Find(id);
            if (runner == null)
            {
                return HttpNotFound();
            }
            return View(runner);
        }

        // POST: Runners/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Runner runner = db.Runners.Find(id);
            db.Runners.Remove(runner);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
