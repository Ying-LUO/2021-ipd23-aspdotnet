﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RaceRegistration.Models
{
    public class Event
    {
        public int EventId { get; set; }

        [StringLength(255, ErrorMessage = "Event Name cannot be longer than 255 characters.")]
        public string Name { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Event Date")]
        public DateTime? EventDate { get; set; }

        public bool IsClosed { get; set; }
    }
}