﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RaceRegistration.Models
{
    public class Country
    {
        public int CountryId { get; set; }

        [StringLength(255, ErrorMessage = "Country Name cannot be longer than 255 characters.")]
        public string Name { get; set; }

    }
}