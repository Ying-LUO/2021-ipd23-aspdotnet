﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RaceRegistration.Models
{
    public class Runner
    {
        public int RunnerId { get; set; }

        [StringLength(100, ErrorMessage = "FirstName cannot be longer than 100 characters.")]
        public string FirstName { get; set; }

        [StringLength(100, ErrorMessage = "LastName cannot be longer than 100 characters.")]
        public string LastName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Birth Date")]
        public DateTime? BirthDate { get; set; }

        public Gender Gender { get; set; }

        [StringLength(50, ErrorMessage = "Email cannot be longer than 50 characters.")]
        public string Email { get; set; }

        [StringLength(15, ErrorMessage = "Telephone cannot be longer than 15 characters.")]
        public string Telephone { get; set; }

        [StringLength(255, ErrorMessage = "Address cannot be longer than 255 characters.")]
        public string Address { get; set; }

        [StringLength(50, ErrorMessage = "City cannot be longer than 50 characters.")]
        public string City { get; set; }

        [StringLength(15, ErrorMessage = "PostalCode cannot be longer than 15 characters.")]
        public string PostalCode { get; set; }

        [Required]
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Registration Date")]
        public DateTime? RegistrationDate { get; set; }

        public string ContactPersonName { get; set; }

        public string ContactPersonTelephone { get; set; }

        [Required]
        public int EventId { get; set; }

        public virtual Event Event { get; set; }

    }
}