﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineAdmissionSystem
{
    public class MyHub : Hub
    {
        public static void UpdateFromServer()
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<MyHub>();

            hubContext.Clients.All.updateClient();
        }
    }
}