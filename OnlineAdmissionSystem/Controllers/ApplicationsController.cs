﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using OnlineAdmissionSystem.Helpers;
using OnlineAdmissionSystem.Models;
using PagedList;

namespace OnlineAdmissionSystem.Controllers
{
    public class ApplicationsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Applications
        //public ActionResult Index()currentFilter
        public ActionResult Index(string sortDir, string searchString, string currentFilter, int? page, string sortOrder = "")
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.currentFilter = searchString;
            ViewBag.sortOrder = sortOrder;
            ViewBag.sortDir = sortDir;

            var applications = db.Applications.Include(a => a.Department).Include(a => a.Program).Include(a => a.User).AsQueryable();

            if (!string.IsNullOrEmpty(searchString))
            {
                applications = applications.Where(p => p.FirstName.ToLower().Contains(searchString.ToLower())||
                p.LastName.ToLower().Contains(searchString.ToLower()) || 
                p.User.Email.ToLower().Contains(searchString.ToLower()) ||
                p.Department.Name.ToLower().Contains(searchString.ToLower()) ||
                p.Program.Name.ToLower().Contains(searchString.ToLower())
                );
            }

            switch (sortOrder.ToLower())
            {
                case "email":
                    if (sortDir.ToLower() == "desc")
                        applications = applications.OrderByDescending(s => s.User.Email);
                    else
                        applications = applications.OrderBy(s => s.User.Email);
                    break;
                case "firstname":
                    if (sortDir.ToLower() == "desc")
                        applications = applications.OrderByDescending(s => s.FirstName);
                    else
                        applications = applications.OrderBy(s => s.FirstName);
                    break;
                case "lastname":
                    if (sortDir.ToLower() == "desc")
                        applications = applications.OrderByDescending(s => s.LastName);
                    else
                        applications = applications.OrderBy(s => s.LastName);
                    break;
                case "telephonenumber":
                    if (sortDir.ToLower() == "desc")
                        applications = applications.OrderByDescending(s => s.TelePhoneNumber);
                    else
                        applications = applications.OrderBy(s => s.TelePhoneNumber);
                    break;
                case "department":
                    if (sortDir.ToLower() == "desc")
                        applications = applications.OrderByDescending(s => s.DepartmentId);
                    else
                        applications = applications.OrderBy(s => s.DepartmentId);
                    break;
                case "program":
                    if (sortDir.ToLower() == "desc")
                        applications = applications.OrderByDescending(s => s.ProgramId);
                    else
                        applications = applications.OrderBy(s => s.ProgramId);
                    break;
                case "registrationdate":
                    if (sortDir.ToLower() == "desc")
                        applications = applications.OrderByDescending(s => s.RegistrationDate);
                    else
                        applications = applications.OrderBy(s => s.RegistrationDate);
                    break;
                case "status":
                    if (sortDir.ToLower() == "desc")
                        applications = applications.OrderByDescending(s => s.Status);
                    else
                        applications = applications.OrderBy(s => s.Status);
                    break;
                default:
                    applications = applications.OrderBy(s => s.FirstName);
                    break;
            }

            //var applications = db.Applications.Include(a => a.Department).Include(a => a.Program).Include(a => a.User);
            if (!User.IsInRole(RoleName.CanManage))
            {
                var Id = db.Users.Where(u => u.Email == User.Identity.Name).SingleOrDefault().Id;
                applications = db.Applications.Include(a => a.Department).Include(a => a.Program).Where(a => a.ApplicationId == Id);
                return View("SingleApplicationView", applications);
            }

            int pageSize = 2;
            int pageNumber = (page ?? 1);
            var data = applications.ToPagedList(pageNumber, pageSize);
            if (Request.IsAjaxRequest())
            {
                return this.PartialView("_SearchList", data);
            }
            else
            {
                return View(data);
            }

            //return View(applications.ToList());
        }

        // GET: Applications/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            application.Program = db.Programs.Where(p => p.ProgramId == application.ProgramId).SingleOrDefault();
            application.Department = db.Departments.Where(d => d.DepartmentId == application.Program.DepartmentId).SingleOrDefault();
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }


        public ActionResult EducationDetails(string id)
        {
            ViewBag.Id = id;
            var educationDetails = db.EducationDetails.Include(e => e.Application).Where(e => e.ApplicationId == id);
            return View(educationDetails.ToList());
        }

        public ActionResult EnclosedDocuments(string id)
        {
            ViewBag.Id = id;
            var enclosedDocuments = db.EnclosedDocuments.Include(e => e.Application).Where(e => e.ApplicationId == id);
            return View(enclosedDocuments.ToList());
        }

        // GET: Applications/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Name");
            ViewBag.ProgramId = new SelectList(db.Programs, "ProgramId", "Name");
            if (User.IsInRole(RoleName.CanManage))
            {
                ViewBag.ApplicationId = new SelectList(db.Users.Where(u => u.Email != null), "Id", "Email");
            }
            else
            {
                ViewBag.ApplicationId = new SelectList(db.Users.Where(u => u.Email == User.Identity.Name), "Id", "Email"); ;
            }
            return View();
        }

        // POST: Applications/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ApplicationId,FirstName,LastName,TelePhoneNumber,Photo,DepartmentId,ProgramId,RegistrationDate,Status")] ApplicationViewModel applicationVM)
        {
            if (ModelState.IsValid)
            {
                //Mapper.CreateMap<ApplicationViewModel, Application>();
                //var application = Mapper.Map<Application>(applicationVM);
                var application = new Application();
                application.ApplicationId = applicationVM.ApplicationId;
                application.ProgramId = applicationVM.ProgramId;
                application.FirstName = applicationVM.FirstName;
                application.LastName = applicationVM.LastName;
                application.TelePhoneNumber = applicationVM.TelePhoneNumber;
                application.RegistrationDate = DateTime.Now;
                application.ProgramId = applicationVM.ProgramId;
                application.DepartmentId = db.Programs.Where(p => p.ProgramId == applicationVM.ProgramId).FirstOrDefault().DepartmentId;
                application.Status = Status.Draft;
                if (applicationVM.Photo != null)
                    application.Photo = ImageConverter.ByteArrayFromPostedFile(applicationVM.Photo);
                db.Applications.Add(application);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Name", applicationVM.DepartmentId);
            ViewBag.ProgramId = new SelectList(db.Programs, "ProgramId", "Name", applicationVM.ProgramId);
            ViewBag.ApplicationId = new SelectList(db.Users, "Id", "Email", applicationVM.ApplicationId);
            return View(applicationVM);
        }

        // GET: Applications/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Name", application.DepartmentId);
            ViewBag.ProgramId = new SelectList(db.Programs, "ProgramId", "Name", application.ProgramId);
            if (User.IsInRole(RoleName.CanManage))
            {
                ViewBag.ApplicationId = new SelectList(db.Users.Where(u => u.Email != null), "Id", "Email");
            }
            else
            {
                ViewBag.ApplicationId = new SelectList(db.Users.Where(u => u.Email == User.Identity.Name), "Id", "Email"); ;
            }

            Mapper.CreateMap<Application, ApplicationViewModel>().ForMember(x => x.Photo, opt => opt.Ignore());
            var applicationVM = Mapper.Map<ApplicationViewModel>(application);
            applicationVM.PhotoDB = application.Photo;
            return View(applicationVM);
        }

        // POST: Applications/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ApplicationId,FirstName,LastName,TelePhoneNumber,Photo,DepartmentId,ProgramId,RegistrationDate,Status")] Application application)
        public ActionResult Edit(ApplicationViewModel applicationVM)
        {
            if (ModelState.IsValid)
            {
                Application application = db.Applications.Find(applicationVM.ApplicationId);
                if (applicationVM != null && applicationVM.Photo != null)
                    application.Photo = ImageConverter.ByteArrayFromPostedFile(applicationVM.Photo);
                application.FirstName = applicationVM.FirstName;
                application.LastName = applicationVM.LastName;
                application.ProgramId = applicationVM.ProgramId;
                application.DepartmentId = db.Programs.Where(p => p.ProgramId == applicationVM.ProgramId).FirstOrDefault().DepartmentId;
                application.TelePhoneNumber = applicationVM.TelePhoneNumber;
                application.Status = applicationVM.Status;

                db.Entry(application).State = EntityState.Modified;
                db.SaveChanges();
                MyHub.UpdateFromServer();
                return RedirectToAction("Index");
            }
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Name", applicationVM.DepartmentId);
            ViewBag.ProgramId = new SelectList(db.Programs, "ProgramId", "Name", applicationVM.ProgramId);
            ViewBag.ApplicationId = new SelectList(db.Users, "Id", "Email", applicationVM.ApplicationId);
            return View(applicationVM);
        }

        // GET: Applications/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            application.Program = db.Programs.Where(p => p.ProgramId == application.ProgramId).SingleOrDefault();
            application.Department = db.Departments.Where(d => d.DepartmentId == application.Program.DepartmentId).SingleOrDefault();
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // POST: Applications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Application application = db.Applications.Find(id);
            db.Applications.Remove(application);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
