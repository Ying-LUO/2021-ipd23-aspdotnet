﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using OnlineAdmissionSystem.Models;

namespace OnlineAdmissionSystem.Controllers
{
    public class EnclosedDocumentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: EnclosedDocuments
        public ActionResult Index()
        {
            var enclosedDocuments = db.EnclosedDocuments.Include(e => e.Application);
            return View(enclosedDocuments.ToList());
        }

        // GET: EnclosedDocuments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EnclosedDocument enclosedDocument = db.EnclosedDocuments.Find(id);
            if (enclosedDocument == null)
            {
                return HttpNotFound();
            }
            return View(enclosedDocument);
        }

        // GET: EnclosedDocuments/Create
        public ActionResult Create()
        {
            var Id = db.Users.Where(u => u.Email == User.Identity.Name).SingleOrDefault().Id;
            ViewBag.ApplicationId = new SelectList(db.Applications.Where(a=>a.ApplicationId == Id), "ApplicationId", "FirstName");
            return View();
        }

        // POST: EnclosedDocuments/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,ApplicationId,DocumentType,Name,DocumentFile")] EnclosedDocument enclosedDocument)
        public ActionResult Create([Bind(Include = "Id,ApplicationId,DocumentType,Name,DocumentFile")] EnclosedDocumentViewModel enclosedDocumentVM)
        {
            if (ModelState.IsValid)
            {
                var enclosedDocument = new EnclosedDocument();
                enclosedDocument.ApplicationId = enclosedDocumentVM.ApplicationId;
                enclosedDocument.DocumentType = enclosedDocumentVM.DocumentType;
                enclosedDocument.Name = enclosedDocumentVM.Name;
                enclosedDocument.DocumentFile = new byte[enclosedDocumentVM.DocumentFile.InputStream.Length];
                enclosedDocumentVM.DocumentFile.InputStream.Read(enclosedDocument.DocumentFile, 0, enclosedDocument.DocumentFile.Length);

                db.EnclosedDocuments.Add(enclosedDocument);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ApplicationId = new SelectList(db.Applications, "ApplicationId", "FirstName", enclosedDocumentVM.ApplicationId);
            return View(enclosedDocumentVM);
        }

        // GET: EnclosedDocuments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EnclosedDocument enclosedDocument = db.EnclosedDocuments.Find(id);
            if (enclosedDocument == null)
            {
                return HttpNotFound();
            }
            ViewBag.ApplicationId = new SelectList(db.Applications, "ApplicationId", "FirstName", enclosedDocument.ApplicationId);
            return View(enclosedDocument);
        }

        // POST: EnclosedDocuments/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ApplicationId,DocumentType,Name,DocumentFile")] EnclosedDocumentViewModel enclosedDocumentVM)
        {
            if (ModelState.IsValid)
            {
                EnclosedDocument enclosedDocument = db.EnclosedDocuments.Find(enclosedDocumentVM.Id);
                if (enclosedDocumentVM != null && enclosedDocumentVM.DocumentFile != null)
                    enclosedDocument.DocumentFile = new byte[enclosedDocumentVM.DocumentFile.InputStream.Length];
                enclosedDocumentVM.DocumentFile.InputStream.Read(enclosedDocument.DocumentFile, 0, enclosedDocument.DocumentFile.Length);
                enclosedDocument.DocumentType = enclosedDocumentVM.DocumentType;
                enclosedDocument.Name = enclosedDocumentVM.Name;

                db.Entry(enclosedDocument).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ApplicationId = new SelectList(db.Applications, "ApplicationId", "FirstName", enclosedDocumentVM.ApplicationId);
            return View(enclosedDocumentVM);
        }

        // GET: EnclosedDocuments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EnclosedDocument enclosedDocument = db.EnclosedDocuments.Find(id);
            if (enclosedDocument == null)
            {
                return HttpNotFound();
            }
            return View(enclosedDocument);
        }

        // POST: EnclosedDocuments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EnclosedDocument enclosedDocument = db.EnclosedDocuments.Find(id);
            db.EnclosedDocuments.Remove(enclosedDocument);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult ViewDocument(int id)
        {
            EnclosedDocument enclosedDocument = db.EnclosedDocuments.Find(id);
            ViewData["PDF"] = enclosedDocument.DocumentFile;
            return this.PartialView("_DocumentView", ViewData["PDF"]);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
