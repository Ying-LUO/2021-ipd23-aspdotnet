﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineAdmissionSystem.Models
{
    public class Program
    {
        public int ProgramId { get; set; }
        public string Name { get; set; }
        public int DepartmentId { get; set; }
        public Department Department { get; set; }

        public ICollection<Application> Applications { get; set; }

    }
}