﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineAdmissionSystem.Models
{
    public class EnclosedDocumentViewModel
    {
        public int Id { get; set; }
        public string ApplicationId { get; set; }
        public Application Application { get; set; }
        public string DocumentType { get; set; }
        public string Name { get; set; }
        public HttpPostedFileBase DocumentFile { get; set; }
        public byte[] DocumentFileDB { get; set; }

    }
}