﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineAdmissionSystem.Models
{
    public enum Status
    {
        Draft = 1,
        Saved = 2,
        Received = 3,
        Approved = 4
    }
}