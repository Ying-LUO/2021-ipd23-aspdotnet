﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OnlineAdmissionSystem.Models
{
    public class Application
    {
        [ForeignKey("User")]
        [Display(Name = "Email")]
        public string ApplicationId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Display(Name = "Mobile Number")]
        public string TelePhoneNumber { get; set; }
        public byte[] Photo { get; set; }

        [Display(Name = "Department")]
        public int? DepartmentId { get; set; }
        public Department Department { get; set; }

        [Display(Name = "Program")]
        public int? ProgramId { get; set; }
        public Program Program { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Registration Date")]
        public DateTime? RegistrationDate { get; set; }

        //[Column("Status")]
        //public string StatusString
        //{
        //    get { return Status.ToString(); }
        //    private set { Status = (Status)Enum.Parse(typeof(Status), value); }
        //}

        //[NotMapped]
        public Status Status { get; set; }

        public ICollection<EducationDetail> EducationDetails { get; set; }
        public ICollection<EnclosedDocument> EnclosedDocuments { get; set; }

        public virtual ApplicationUser User { get; set; }

    }
}