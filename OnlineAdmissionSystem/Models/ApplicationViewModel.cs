﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineAdmissionSystem.Models
{
    public class ApplicationViewModel
    {
        public string ApplicationId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TelePhoneNumber { get; set; }
        public HttpPostedFileBase Photo { get; set; }
        public byte[] PhotoDB { get; set; }

        public int? DepartmentId { get; set; }
        public Department Department { get; set; }

        public int? ProgramId { get; set; }
        public Program Program { get; set; }

        [Display(Name = "Registration Date")]
        public DateTime? RegistrationDate { get; set; }

        //[Column("Status")]
        //public string StatusString
        //{
        //    get { return Status.ToString(); }
        //    private set { Status = (Status)Enum.Parse(typeof(Status), value); }
        //}

        //[NotMapped]
        public Status Status { get; set; }

        public virtual ApplicationUser User { get; set; }

    }
}