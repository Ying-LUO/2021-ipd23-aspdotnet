﻿using Day03FirstMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Day03FirstMVC.Controllers
{
    [HandleError]
    public class StudentController : Controller
    {
        static IList<Student> studentList = new List<Student>{
                new Student() { StudentId = 1, StudentName = "John", Age = 18 } ,
                new Student() { StudentId = 2, StudentName = "Steve",  Age = 21 } ,
                new Student() { StudentId = 3, StudentName = "Bill",  Age = 25 } ,
                new Student() { StudentId = 4, StudentName = "Ram" , Age = 20 } ,
                new Student() { StudentId = 5, StudentName = "Ron" , Age = 31 } ,
                new Student() { StudentId = 4, StudentName = "Chris" , Age = 17 } ,
                new Student() { StudentId = 4, StudentName = "Rob" , Age = 19 }
            };

        // GET: Student
        public ActionResult Index() // handles GET requests by default
        {
            //return View(studentList.OrderByDescending(s => s.Age).ToList());
            ViewBag.TotalStudents = studentList.Count();
            ViewData["students"] = studentList;
            return View(studentList.OrderBy(s => s.StudentId).ToList());
        }

        public ActionResult Edit(int id)
        {
            // update student to the database
            var std = studentList.Where(s => s.StudentId == id).FirstOrDefault();
            return View(std);
        }

        [HttpPost]
        public ActionResult Edit(Student std)
        {
            if (ModelState.IsValid)
            {
                var name = std.StudentName;
                var age = std.Age;

                var originStd = studentList.Where(s => s.StudentId == std.StudentId).FirstOrDefault();
                var nameList = studentList.Where(s => s.StudentId != std.StudentId).Select(s => s.StudentName).ToList();

                bool nameAlreadyExists = nameList.Contains(name);

                if (nameAlreadyExists)
                {
                    //adding error message to ModelState
                    ModelState.AddModelError("name", "Student Name Already Exists.");

                    return View(std);
                }

                originStd.Age = age;
                originStd.StudentName = name;

                return RedirectToAction("Index");
            }
            return View(std);
        }

        [HttpPost]
        public ActionResult Create(Student std)
        {
            if (ModelState.IsValid)
            {
                var name = std.StudentName;
                var age = std.Age;

                var lastId = studentList.OrderBy(s => s.StudentId).ToList().Last().StudentId;
                var nameList = studentList.Select(s => s.StudentName).ToList();

                bool nameAlreadyExists = nameList.Contains(name);

                if (nameAlreadyExists)
                {
                    //adding error message to ModelState
                    ModelState.AddModelError("name", "Student Name Already Exists.");

                    return View(std);
                }

                var newStd = new Student() { StudentId = lastId + 1, StudentName = std.StudentName, Age = std.Age };
                studentList.Add(newStd);

                return RedirectToAction("Index");
            }
            return View(std);
        }

        public ActionResult Create()
        {
            return View();
        }

        //[HttpDelete]
        [HttpPost]
        public ActionResult Delete(Student std)
        {
            studentList.Remove(std);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var std = studentList.Where(s => s.StudentId == id).FirstOrDefault();
            return View(std);
        }

        public ActionResult Details(int id) 
        {
            var std = studentList.Where(s => s.StudentId == id).FirstOrDefault();
            return View(std);
        }


        [HttpPut]
        public ActionResult PutAction() // handles PUT requests by default
        {
            return View("Index");
        }

        [HttpHead]
        public ActionResult HeadAction() // handles HEAD requests by default
        {
            return View("Index");
        }

        [HttpOptions]
        public ActionResult OptionsAction() // handles OPTION requests by default
        {
            return View("Index");
        }

        [HttpPatch]
        public ActionResult PatchAction() // handles PATCH requests by default
        {
            return View("Index");
        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult GetAndPostAction()
        {
            return RedirectToAction("Index");
        }


    }
}