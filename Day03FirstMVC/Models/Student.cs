﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Day03FirstMVC.Models
{
    public class Student
    {
        public int StudentId { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please enter student name.")]
        public string StudentName { get; set; }
        [Required(ErrorMessage = "Please enter student age.")]
        [Range(10, 40)]
        public int Age { get; set; }
        public Standard standard { get; set; }
        public bool isNewlyEnrolled { get; set; }
        public string Password { get; set; }
    }

    public class Standard
    {
        public int StandardId { get; set; }
        public string StandardName { get; set; }
    }
}