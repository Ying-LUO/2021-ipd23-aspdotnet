namespace Day02DataBindinf.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Day02DataBindinf.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Day02DataBindinf.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.Products.AddOrUpdate(
                new Models.Product() { Name="Paper", Description=""},
                new Models.Product() { Name = "Notebook", Description = "" },
                new Models.Product() { Name = "Pencil", Description = "Package of 12 pencils" }
                );
            context.SaveChanges();
        }
    }
}
