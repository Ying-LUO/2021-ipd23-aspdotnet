﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Day02DataBindinf.Startup))]
namespace Day02DataBindinf
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
