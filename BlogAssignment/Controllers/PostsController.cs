﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using BlogAssignment.Helper;
using BlogAssignment.Models;
using PagedList;

namespace BlogAssignment.Controllers
{
    public class PostsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Posts
        public ActionResult Index(string sortDir, string searchString, string currentFilter, int? page, string sortOrder="")
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.currentFilter = searchString;
            ViewBag.sortOrder = sortOrder;
            ViewBag.sortDir = sortDir;

            var posts = db.Posts.AsQueryable();

            if (!string.IsNullOrEmpty(searchString))
            {
                posts = posts.Where(p => p.Content.Contains(searchString));
            }

            
            switch (sortOrder.ToLower())
            {
                case "title":
                    if (sortDir.ToLower() == "desc")
                        posts = posts.OrderByDescending(s => s.Title);
                    else
                        posts = posts.OrderBy(s => s.Title);
                    break;
                case "content":
                    if (sortDir.ToLower() == "desc")
                        posts = posts.OrderByDescending(s => s.Content);
                    else
                        posts = posts.OrderBy(s => s.Content);
                    break;
                case "createdon":
                    if (sortDir.ToLower() == "desc")
                        posts = posts.OrderByDescending(s => s.CreatedOn);
                    else
                        posts = posts.OrderBy(s => s.CreatedOn);
                    break;
                case "updatedon":
                    if (sortDir.ToLower() == "desc")
                        posts = posts.OrderByDescending(s => s.UpdatedOn);
                    else
                        posts = posts.OrderBy(s => s.UpdatedOn);
                    break;
                case "postedon":
                    if (sortDir.ToLower() == "desc")
                        posts = posts.OrderByDescending(s => s.PostedOn);
                    else
                        posts = posts.OrderBy(s => s.PostedOn);
                    break;
                case "userfullname":
                    if (sortDir.ToLower() == "desc")
                        posts = posts.OrderByDescending(s => s.UserFullName);
                    else
                        posts = posts.OrderBy(s => s.UserFullName);
                    break;
                default:
                    posts = posts.OrderBy(s => s.Title);
                    break;
            }

            int pageSize = 2;
            int pageNumber = (page ?? 1);
            var data = posts.ToPagedList(pageNumber, pageSize);
            if (Request.IsAjaxRequest())
            {
                return View("_SearchList", data);
            }
            else
            {
                return View(data);
            }
        }

        // GET: Posts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // GET: Posts/Create
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Posts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Content,CreatedOn,UpdatedOn,PostedOn,UserFullName,Photo")] PostViewModel postVM)
        {
            if (ModelState.IsValid)
            {
                //Mapper.CreateMap<PostViewModel, Post>();
                //var post = Mapper.Map<Post>(postVM);
                var post = new Post();
                post.Title = postVM.Title;
                post.Content = postVM.Content;
                post.CreatedOn = postVM.CreatedOn;
                post.UpdatedOn = postVM.UpdatedOn;
                post.PostedOn = postVM.PostedOn;
                post.UserFullName = postVM.UserFullName;
                if (postVM.Photo != null)
                    post.Photo = ImageConverter.ByteArrayFromPostedFile(postVM.Photo);

                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(postVM);
        }

        // GET: Posts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            Mapper.CreateMap<Post, PostViewModel>().ForMember(x => x.Photo, opt => opt.Ignore());
            var postVM = Mapper.Map<PostViewModel>(post);
            postVM.PhotoDB = post.Photo;
            return View(postVM);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,Title,Content,CreatedOn,UpdatedOn,PostedOn,UserFullName,Photo")] Post post)
        public ActionResult Edit(PostViewModel postVM)
        {
            if (ModelState.IsValid)
            {
                Post post = db.Posts.Find(postVM.Id);
                if (postVM != null && postVM.Photo != null)
                    post.Photo = ImageConverter.ByteArrayFromPostedFile(postVM.Photo);
                post.Title = postVM.Title;
                post.Content = postVM.Content;
                post.CreatedOn = postVM.CreatedOn;
                post.UpdatedOn = postVM.UpdatedOn;
                post.PostedOn = postVM.PostedOn;
                post.UserFullName = postVM.UserFullName;
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(postVM);
        }

        // GET: Posts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
