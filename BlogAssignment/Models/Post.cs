﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlogAssignment.Models
{
    public class Post
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please input blog title", AllowEmptyStrings = false)]
        [StringLength(255, ErrorMessage = "Blog title cannot be longer than 255 characters.")]
        public string Title { get; set; }

        public string Content { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Creation Date")]
        public DateTime CreatedOn { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Modification Date")]
        public DateTime UpdatedOn { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Publication Date")]
        public DateTime? PostedOn { get; set; }

        [StringLength(255, ErrorMessage = "User name cannot be longer than 255 characters.")]
        //[RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$", ErrorMessage = "Invalid User Name")]
        [Display(Name = "Posted By")]
        [Required]
        public string UserFullName { get; set; }

        public byte[] Photo { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
    }
}